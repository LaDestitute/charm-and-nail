package com.ladestitute.charmandnail.util.datagen.loot.mob;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.ladestitute.charmandnail.registry.ItemInit;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;

import java.util.List;
import java.util.Random;

public class MooshroomSporeShroomLootModifier extends LootModifier {
    public MooshroomSporeShroomLootModifier(final LootItemCondition[] conditionsIn, final float chance){
        super(conditionsIn);
    }

    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
        Random rand = new Random();
        int spore_shroom = rand.nextInt(101);
        if (spore_shroom <= 10) {
          //  generatedLoot.add(new ItemStack(ItemInit.SPORE_SHROOM.get(), 1));
        }

        return generatedLoot;
    }

    public static final class Serializer extends GlobalLootModifierSerializer<MooshroomSporeShroomLootModifier> {
        @Override public MooshroomSporeShroomLootModifier read(ResourceLocation location, JsonObject object, LootItemCondition[] conditions){
            final float chance = GsonHelper.getAsFloat(object, "chance", 0.1F);
            if (chance <= 0) throw new JsonParseException("Unable to set a chance to a number lower than 1");
            return new MooshroomSporeShroomLootModifier(conditions, chance);
        }
        @Override public JsonObject write(MooshroomSporeShroomLootModifier instance){
            return makeConditions(instance.conditions);
        }
    }
}

