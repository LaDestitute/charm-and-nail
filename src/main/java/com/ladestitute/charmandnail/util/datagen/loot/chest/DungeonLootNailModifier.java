package com.ladestitute.charmandnail.util.datagen.loot.chest;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.ladestitute.charmandnail.registry.ItemInit;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;

import java.util.List;
import java.util.Random;

public class DungeonLootNailModifier extends LootModifier {
    private final float chance;

    public DungeonLootNailModifier(final LootItemCondition[] conditionsIn, final float chance) {
        super(conditionsIn);
        this.chance = chance;
    }

    public DungeonLootNailModifier(final LootItemCondition[] conditionsIn) {
        this(conditionsIn, 0.1F);
    }

    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
        Random rand = new Random();
        int nail = rand.nextInt(101);
        int fragileheart = rand.nextInt(101);
        int fragilestrength = rand.nextInt(101);
        int heavyblow = rand.nextInt(101);
        int longnail = rand.nextInt(101);
        int quickslash = rand.nextInt(101);
        int steadybody = rand.nextInt(101);
        if (nail <= 20) {
            generatedLoot.add(new ItemStack(ItemInit.OLD_NAIL.get()));
        }
        if (fragileheart <= 10) {
            generatedLoot.add(new ItemStack(ItemInit.FRAGILE_HEART.get()));
        }
        if (fragilestrength <= 10) {
            generatedLoot.add(new ItemStack(ItemInit.FRAGILE_STRENGTH.get()));
        }
        if (heavyblow <= 5) {
            generatedLoot.add(new ItemStack(ItemInit.HEAVY_BLOW.get()));
        }
        if (longnail <= 5) {
            generatedLoot.add(new ItemStack(ItemInit.LONGNAIL.get()));
        }
        if (quickslash <= 5) {
            generatedLoot.add(new ItemStack(ItemInit.QUICK_SLASH.get()));
        }
        if (steadybody <= 5) {
            generatedLoot.add(new ItemStack(ItemInit.STEADY_BODY.get()));
        }
        return generatedLoot;
    }

    public static class Serializer extends GlobalLootModifierSerializer<DungeonLootNailModifier> {
        @Override
        public DungeonLootNailModifier read(ResourceLocation location, JsonObject object, LootItemCondition[] conditions) {
            final float chance = GsonHelper.getAsFloat(object, "chance", 0.1F);
            if (chance <= 0) throw new JsonParseException("Unable to set a chance to a number lower than 1");
            return new DungeonLootNailModifier(conditions, chance);
        }

        @Override
        public JsonObject write(DungeonLootNailModifier instance) {
            final JsonObject obj = this.makeConditions(instance.conditions);
            obj.addProperty("chance", instance.chance);
            return obj;
        }
    }
}