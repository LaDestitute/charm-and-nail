package com.ladestitute.charmandnail.util.datagen;

import com.ladestitute.charmandnail.CharmAndNailMain;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;

public class CharmAndNailTagsList {
    public static class ItemTagsDataGen extends ItemTagsProvider {

        public ItemTagsDataGen(DataGenerator generatorIn, BlockTagsProvider blockTagsProvider,
                               ExistingFileHelper existingFileHelper) {
            super(generatorIn, blockTagsProvider, CharmAndNailMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
            tag(CharmAndNailTags.Items.TEMPLATE_TAG_LIST).add(Items.AMETHYST_BLOCK);
            tag(CharmAndNailTags.Items.TEMPLATE_TAG_LIST).add(Items.AMETHYST_SHARD);

        }
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, CharmAndNailMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
        }
    }
}
