package com.ladestitute.charmandnail.util.datagen;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.util.datagen.loot.chest.DungeonLootNailModifier;
import com.ladestitute.charmandnail.util.datagen.loot.mob.*;
import com.ladestitute.charmandnail.util.datagen.loot.mob.witch.WitchShamanStoneLootModifier;
import com.ladestitute.charmandnail.util.datagen.loot.mob.witch.WitchSoulCatcherLootModifier;
import com.ladestitute.charmandnail.util.glm.CharmAndNailDropModifier;
import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.storage.loot.predicates.ExplosionCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.data.GlobalLootModifierProvider;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootTableIdCondition;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CharmAndNailDataGen
{
    public static final DeferredRegister<GlobalLootModifierSerializer<?>> GLM = DeferredRegister.create(ForgeRegistries.LOOT_MODIFIER_SERIALIZERS, CharmAndNailMain.MOD_ID);
    public static final RegistryObject<DungeonLootNailModifier.Serializer> DUNGEON_LOOT =
            GLM.register("dungeon_loot", DungeonLootNailModifier.Serializer::new);
    public static final RegistryObject<WitchSoulCatcherLootModifier.Serializer> WITCH_SOUL_CATCHER =
            GLM.register("witch_soul_catcher", WitchSoulCatcherLootModifier.Serializer::new);
    public static final RegistryObject<WitchShamanStoneLootModifier.Serializer> WITCH_SHAMAN_STONE =
            GLM.register("witch_shaman_stone", WitchShamanStoneLootModifier.Serializer::new);
    public static final RegistryObject<ZombieFuryOfTheFallenLootModifier.Serializer> ZOMBIE_FOTF =
            GLM.register("zombie_fotf", ZombieFuryOfTheFallenLootModifier.Serializer::new);
    public static final RegistryObject<SkeletonFuryOfTheFallenLootModifier.Serializer> SKELETON_FOTF =
            GLM.register("skeleton_fotf", SkeletonFuryOfTheFallenLootModifier.Serializer::new);
    public static final RegistryObject<HuskFuryOfTheFallenLootModifier.Serializer> HUSK_FOTF =
            GLM.register("husk_fotf", HuskFuryOfTheFallenLootModifier.Serializer::new);
    public static final RegistryObject<StrayFuryOfTheFallenLootModifier.Serializer> STRAY_FOTF =
            GLM.register("stray_fotf", StrayFuryOfTheFallenLootModifier.Serializer::new);
    public static final RegistryObject<IronGolemSteadyBodyLootModifier.Serializer> IRON_GOLEM_STEADY_BODY =
            GLM.register("iron_golem_steady_body", IronGolemSteadyBodyLootModifier.Serializer::new);
    public static final RegistryObject<MooshroomSporeShroomLootModifier.Serializer> MOOSHROOM_SPORE_SHROOM =
            GLM.register("mooshroom_spore_shroom", MooshroomSporeShroomLootModifier.Serializer::new);

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event)
    {
        DataGenerator generator = event.getGenerator();

        if (event.includeServer())
        {
            CharmAndNailMain.LOGGER.debug("Starting Server Data Generators");
            CharmAndNailTagsList.BlockTagsDataGen blockTagsProvider = new CharmAndNailTagsList.BlockTagsDataGen(event.getGenerator(), event.getExistingFileHelper());
            // generator.addProvider(new RecipeOverrides(generator));
            // generator.addProvider(new LootTableDataGen(generator));
            generator.addProvider(new CharmAndNailTagsList.ItemTagsDataGen(generator, blockTagsProvider, event.getExistingFileHelper()));
            generator.addProvider(new GLMProvider(generator));
        }
        if (event.includeClient())
        {
            CharmAndNailMain.LOGGER.debug("Starting Client Data Generators");
        }
        generator.addProvider(new LootModifier(generator));
    }

    private static class GLMProvider extends GlobalLootModifierProvider {

        public GLMProvider(DataGenerator gen) {
            super(gen, CharmAndNailMain.MOD_ID);
        }

        @Override
        protected void start() {
            add("charmandnail_drops", CharmAndNailDropModifier.CHARMANDNAIL_DROPS.get(), new CharmAndNailDropModifier.BlockDropModifier(
                    new LootItemCondition[] {
                            ExplosionCondition.survivesExplosion().build()
                    }));

        }
    }

    private static class LootModifier extends GlobalLootModifierProvider {
        public LootModifier(DataGenerator gen) {
            super(gen, CharmAndNailMain.MOD_ID);
        }

        @Override
        protected void start() {
            add("dungeon_loot", DUNGEON_LOOT.get(), new DungeonLootNailModifier(
                    new LootItemCondition[] { LootTableIdCondition.builder(new ResourceLocation("chests/simple_dungeon")).build() })
            );
        }
    }
}
