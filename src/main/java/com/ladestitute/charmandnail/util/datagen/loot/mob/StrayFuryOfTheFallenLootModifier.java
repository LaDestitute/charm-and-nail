package com.ladestitute.charmandnail.util.datagen.loot.mob;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.ladestitute.charmandnail.registry.ItemInit;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;

import java.util.List;
import java.util.Random;

public class StrayFuryOfTheFallenLootModifier extends LootModifier {
    public StrayFuryOfTheFallenLootModifier(final LootItemCondition[] conditionsIn, final float chance){
        super(conditionsIn);
    }

    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
        Random rand = new Random();
        int fury_of_the_fallen = rand.nextInt(101);
        if (fury_of_the_fallen <= 40) {
            generatedLoot.add(new ItemStack(ItemInit.FURY_OF_THE_FALLEN.get(), 1));
        }

        return generatedLoot;
    }

    public static final class Serializer extends GlobalLootModifierSerializer<StrayFuryOfTheFallenLootModifier> {
        @Override public StrayFuryOfTheFallenLootModifier read(ResourceLocation location, JsonObject object, LootItemCondition[] conditions){
            final float chance = GsonHelper.getAsFloat(object, "chance", 0.1F);
            if (chance <= 0) throw new JsonParseException("Unable to set a chance to a number lower than 1");
            return new StrayFuryOfTheFallenLootModifier(conditions, chance);
        }
        @Override public JsonObject write(StrayFuryOfTheFallenLootModifier instance){
            return makeConditions(instance.conditions);
        }
    }
}

