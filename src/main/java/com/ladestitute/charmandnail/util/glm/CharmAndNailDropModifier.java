package com.ladestitute.charmandnail.util.glm;

import com.google.gson.JsonObject;
import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.registry.BlockInit;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Random;

public class CharmAndNailDropModifier {
    public static final DeferredRegister<GlobalLootModifierSerializer<?>> GLM = DeferredRegister.create(ForgeRegistries.LOOT_MODIFIER_SERIALIZERS, CharmAndNailMain.MOD_ID);
    public static final RegistryObject<BlockDropModifier.Serializer> CHARMANDNAIL_DROPS = GLM.register("charmandnail_drops", BlockDropModifier.Serializer::new);

    public static class BlockDropModifier extends LootModifier {

        public BlockDropModifier(LootItemCondition[] lootConditions) {
            super(lootConditions);
        }

        @Nonnull
        @Override
        protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
            if (context.hasParam(LootContextParams.BLOCK_STATE)) {
                BlockState state = context.getParamOrNull(LootContextParams.BLOCK_STATE);
                Block block = state.getBlock();
                //Example drop modifier
                Random rand = new Random();
                //next int counts from zero and ignores the last number so you will only get possible results of 0-4 so keep this in mind
                int thornsofagonydropchance = rand.nextInt(101);
                int sporeshroomdropchance = rand.nextInt(101);
                int nethersporeshroomdropchance = rand.nextInt(101);
                int deepfocusdropchance = rand.nextInt(101);
                int quickfocusdropchance = rand.nextInt(101);
                if(block == Blocks.AMETHYST_CLUSTER)
                {
                   if(deepfocusdropchance <= 10)
                   {
                       generatedLoot.add(ItemInit.DEEP_FOCUS.get().getDefaultInstance());
                   }
                    if(quickfocusdropchance <= 1)
                    {
                        generatedLoot.add(ItemInit.QUICK_FOCUS.get().getDefaultInstance());
                    }
                }
                if(block == Blocks.MUSHROOM_STEM||
                block == Blocks.BROWN_MUSHROOM_BLOCK||
                block == Blocks.RED_MUSHROOM_BLOCK)
                {
                    if(sporeshroomdropchance <= 10)
                    {
                    //    generatedLoot.add(ItemInit.SPORE_SHROOM.get().getDefaultInstance());
                    }
                }
                if(block == Blocks.CRIMSON_STEM||
                block == Blocks.WARPED_STEM||block == Blocks.NETHER_WART_BLOCK||
                block == Blocks.WARPED_WART_BLOCK)
                {
                    if(nethersporeshroomdropchance <= 20)
                    {
                     //   generatedLoot.add(ItemInit.SPORE_SHROOM.get().getDefaultInstance());
                    }
                }
                if(block == Blocks.CACTUS||
                        block == Blocks.SWEET_BERRY_BUSH)
                {
                    if(thornsofagonydropchance <= 10)
                    {
                        generatedLoot.add(ItemInit.THORNS_OF_AGONY.get().getDefaultInstance());
                    }
                }
                //
            }
            return generatedLoot;
        }

        private static class Serializer extends GlobalLootModifierSerializer<BlockDropModifier> {

            @Override
            public BlockDropModifier read(ResourceLocation location, JsonObject jsonObject, LootItemCondition[] lootConditions) {
                return new BlockDropModifier(lootConditions);
            }

            @Override
            public JsonObject write(BlockDropModifier instance) {
                return makeConditions(instance.conditions);
            }
        }
    }
}

