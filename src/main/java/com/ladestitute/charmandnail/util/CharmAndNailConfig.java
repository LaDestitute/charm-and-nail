package com.ladestitute.charmandnail.util;

import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

public class CharmAndNailConfig {
    //Example config class
    private static final CharmAndNailConfig INSTANCE;

    public static final ForgeConfigSpec SPEC;

    static {
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
        Pair<CharmAndNailConfig, ForgeConfigSpec> specPair =
                new ForgeConfigSpec.Builder().configure(CharmAndNailConfig::new);
        INSTANCE = specPair.getLeft();
        SPEC = specPair.getRight();
    }

    private final ForgeConfigSpec.BooleanValue turnoffnaturalhealthregen;
    private final ForgeConfigSpec.BooleanValue nailonlysoul;
    private final ForgeConfigSpec.BooleanValue nailcharmseffectmelee;
    private final ForgeConfigSpec.BooleanValue halfthornsofagonydamage;
    private final ForgeConfigSpec.BooleanValue steadybodypreventsmobknockback;
    private final ForgeConfigSpec.IntValue soulmeterposition;

    private CharmAndNailConfig(ForgeConfigSpec.Builder configSpecBuilder) {
        turnoffnaturalhealthregen = configSpecBuilder
                .comment("Turns off natural health regen so the player can only heal via focusing")
                .define("turnoffnaturalhealthregen", true);
        nailonlysoul = configSpecBuilder
                .comment("Only nail weapons gather soul if enabled, if disabled; swords gather soul too")
                .define("nailonlysoul", true);
        nailcharmseffectmelee = configSpecBuilder
                .comment("Whether Fragile Strength, Fury of the Fallen or Thorns of Agony work on swords or the nail only if set to false")
                .define("nailcharmseffectmelee", true);
        halfthornsofagonydamage = configSpecBuilder
                .comment("Halves the damage hostile mobs taken from the Thorns of Agony charm, false will make them take full-damage like in HK")
                .define("halfthornsofagonydamage", true);
        steadybodypreventsmobknockback = configSpecBuilder
                .comment("Whether the charm Steady Body provides immunity to mob-inflicted knockback, otherwise; it will provide immunity to swinging self-recoil")
                .define("steadybodypreventsmobknockback", false);
        soulmeterposition = configSpecBuilder
                .comment("Position of the soul meter, 1 is left, 2 is middle, 3 is right")
                .defineInRange("soulmeterposition", 3, 1, 3);
    }

    public static CharmAndNailConfig getInstance() {
        return INSTANCE;
    }
    // Query operations
    public boolean turnoffnaturalhealthregen() { return turnoffnaturalhealthregen.get(); }
    public boolean nailonlysoul() { return nailonlysoul.get(); }
    public boolean nailcharmseffectmelee() { return nailcharmseffectmelee.get(); }
    public boolean halfthornsofagonydamage() { return halfthornsofagonydamage.get(); }
    public boolean steadybodypreventsmobknockback() { return steadybodypreventsmobknockback.get(); }
    public int soulmeterposition() { return soulmeterposition.get(); }

    //Change operations
    public void changeturnoffnaturalhealthregen(boolean newValue) {
        turnoffnaturalhealthregen.set(newValue);
    }
    public void changenailonlysoul(boolean newValue) {
        nailonlysoul.set(newValue);
    }
    public void changenailcharmseffectmelee(boolean newValue) {
        nailcharmseffectmelee.set(newValue);
    }
    public void changehalfthornsofagonydamage(boolean newValue) {
        halfthornsofagonydamage.set(newValue);
    }
    public void changeteadybodypreventsmobknockback(boolean newValue) {
        steadybodypreventsmobknockback.set(newValue);
    }
    public void changesoulmeterposition(int newValue) {
        soulmeterposition.set(newValue);
    }

    public void save() {
        SPEC.save();
    }
}
