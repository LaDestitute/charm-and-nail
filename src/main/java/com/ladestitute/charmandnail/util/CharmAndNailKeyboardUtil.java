package com.ladestitute.charmandnail.util;

import net.minecraft.client.KeyMapping;
import net.minecraftforge.client.ClientRegistry;
import org.lwjgl.glfw.GLFW;

public class CharmAndNailKeyboardUtil {

    public static KeyMapping focuskey;
    public static KeyMapping spellkey;

    public static void register()
    {
        focuskey = new KeyMapping("focuskey", GLFW.GLFW_KEY_H, "key.categories.charmandnail");
        spellkey = new KeyMapping("spellkey", GLFW.GLFW_KEY_C, "key.categories.charmandnail");

        ClientRegistry.registerKeyBinding(focuskey);
        ClientRegistry.registerKeyBinding(spellkey);
    }

}

