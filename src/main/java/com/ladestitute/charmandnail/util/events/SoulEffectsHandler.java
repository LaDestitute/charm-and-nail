package com.ladestitute.charmandnail.util.events;

import com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability.CharmAndNailSoulCapability;
import com.ladestitute.charmandnail.entities.spells.ShamanVengefulSpiritEntity;
import com.ladestitute.charmandnail.entities.spells.VengefulSpiritEntity;
import com.ladestitute.charmandnail.items.ChannelledNailItem;
import com.ladestitute.charmandnail.items.OldNailItem;
import com.ladestitute.charmandnail.items.SharpenedNailItem;
import com.ladestitute.charmandnail.network.client.ClientboundPlayerSoulUpdateMessage;
import com.ladestitute.charmandnail.network.SimpleNetworkHandler;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.registry.SoundInit;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import com.ladestitute.charmandnail.util.CharmAndNailKeyboardUtil;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.TridentItem;
import net.minecraft.world.level.GameRules;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.network.PacketDistributor;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

public class SoulEffectsHandler {

    @SubscribeEvent
    public void onPlayerEventClone(PlayerEvent.Clone event) {
        boolean flag;
        flag = !(event.getPlayer() instanceof FakePlayer) && event.getPlayer() instanceof ServerPlayer;
        if (flag && event.getPlayer().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).isPresent()) {
            event.getPlayer().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(date -> {
                event.getOriginal().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(t -> {
                    date.setSoul(t.getSoul());
                });
            });
            event.getPlayer().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(t -> SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer) event.getPlayer()), new ClientboundPlayerSoulUpdateMessage(t.getSoul())));
        }
    }

    @SubscribeEvent
    public void configchecklogin(PlayerEvent.PlayerLoggedInEvent event)
    {
        if(CharmAndNailConfig.getInstance().turnoffnaturalhealthregen()) {
            event.getPlayer().level.getGameRules().getRule(GameRules.RULE_NATURAL_REGENERATION)
                    .set(false, event.getPlayer().level.getServer());
        }
        if(!CharmAndNailConfig.getInstance().turnoffnaturalhealthregen()) {
            event.getPlayer().level.getGameRules().getRule(GameRules.RULE_NATURAL_REGENERATION)
                    .set(true, event.getPlayer().level.getServer());
        }

    }

    //If you want your capability to sync to client properly, then you must send it in
    //this event and whenever you modify cap-values in other events
    //You could also alternatively check PlayerLoggedInEvent, PlayerRespawnEvent, PlayerChangedDimensionEvent
    //but that's three times the work, entityjoinworld covers all three
    @SubscribeEvent
    public void onEntityJoinWorld(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof ServerPlayer && !(event.getEntity() instanceof FakePlayer)) {
            event.getEntity().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(t ->
                    SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer) event.getEntity()),
                            new ClientboundPlayerSoulUpdateMessage(t.getSoul())));
        }
    }

    @SubscribeEvent
    public void cancelfocusingifhurt(LivingHurtEvent event)
    {
        if (event.getEntityLiving() instanceof Player) {
            event.getEntityLiving().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(data ->
            {
                data.setFocusing(0);
                data.setFocusingTime(0F);
            });
        }

    }

    @SubscribeEvent
    public void livingDamageEvent(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof Player) {
            event.getSource().getDirectEntity().getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(data ->
            {
                Player player = (Player) event.getSource().getDirectEntity();
                ItemStack soul_catcher =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.SOUL_CATCHER.get(), player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
               // if(CharmAndNailConfig.getInstance().player.getMainHandItem().getItem() instanceof SwordItem) {
               //     data.addSoul((Player) event.getSource().getDirectEntity(), 2);
                //    System.out.println("CAP VALUE IS: " + data.getSoul());
              //  }
                if(player.getMainHandItem().getItem() instanceof OldNailItem ||
                        player.getMainHandItem().getItem() instanceof SharpenedNailItem ||
                        player.getMainHandItem().getItem() instanceof ChannelledNailItem) {
                    if(event.getEntityLiving() instanceof Monster)
                    {
                        if (!soul_catcher.isEmpty()) {
                            data.addSoul((Player) event.getSource().getDirectEntity(), 3);
                            System.out.println("CAP VALUE IS: " + data.getSoul());
                        }
                        else data.addSoul((Player) event.getSource().getDirectEntity(), 2);
                        System.out.println("CAP VALUE IS: " + data.getSoul());
                    }
                }
                if(!CharmAndNailConfig.getInstance().nailonlysoul())
                {
                    if(player.getMainHandItem().getItem() instanceof SwordItem||player.getMainHandItem().getItem() instanceof TridentItem) {
                        if(event.getEntityLiving() instanceof Monster)
                        {
                            if (!soul_catcher.isEmpty()) {
                                data.addSoul((Player) event.getSource().getDirectEntity(), 3);
                                System.out.println("CAP VALUE IS: " + data.getSoul());
                            }
                            else data.addSoul((Player) event.getSource().getDirectEntity(), 2);
                            System.out.println("CAP VALUE IS: " + data.getSoul());
                        }
                    }
                }
                //Codeblock to client-sync, not required if you take care of the syncing in your capability class
                //     if (event.getSource().getDirectEntity() instanceof ServerPlayer && !(event.getSource().getDirectEntity() instanceof FakePlayer)) {
                //      event.getSource().getDirectEntity().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(t ->
                //          SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer) event.getSource().getDirectEntity()),
                //                 new PlayerManaMessage(t.getMana())));
                //   }
            });
        }
    }

    @SubscribeEvent
    public void focusEvent(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(data ->
        {
            //todo: remove private ints in this method and replace them with cap-values for proper multiplayer
            //support
            int still;
            if(data.getSoul() >= 6 && event.player.getHealth() <= 19)
            {
                still = 0;
                if(event.player.xOld != event.player.getX() &&
                        event.player.yOld != event.player.getY() &&
                        event.player.zOld != event.player.getZ())
                {
                    still = 0;
                    data.setFocusingTime(0.01F);
                       System.out.println("FOCUSING IS NOT ACTIVE");
                }
                else still = 1;
                if (still == 1 && CharmAndNailKeyboardUtil.focuskey.isDown()) {
                    data.setFocusing(1);
                    System.out.println("FOCUSING IS ACTIVE");
                }
                if (!CharmAndNailKeyboardUtil.focuskey.isDown()) {
                    data.setFocusing(0);
                    data.setFocusingTime(0F);
                 //   System.out.println("FOCUSING IS NOT ACTIVE");
                }
                    if (data.isFocusing() == 1) {
                        data.addFocusingTime(1F);
                        System.out.println("FOCUSING TIME IS: " + data.getfocustime());
                    }

                ItemStack deep_focus =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.DEEP_FOCUS.get(), event.player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                ItemStack quick_focus =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.QUICK_FOCUS.get(), event.player).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if (!quick_focus.isEmpty() && !deep_focus.isEmpty() && data.getfocustime() >= 19.6F) {
                    data.setFocusing(0);
                    data.subtractSoul(event.player, 6);
                    event.player.addEffect(new MobEffectInstance(MobEffects.HEAL, 5, 1));
                    event.player.playSound(SoundInit.FOCUS_HEAL.get(), 0.5F, 1F);
                    data.setFocusingTime(0F);
                }
                if (!quick_focus.isEmpty() && data.getfocustime() >= 16.88F) {
                    data.setFocusing(0);
                    data.subtractSoul(event.player, 6);
                    event.player.addEffect(new MobEffectInstance(MobEffects.HEAL, 5, 0));
                    event.player.playSound(SoundInit.FOCUS_HEAL.get(), 0.5F, 1F);
                    data.setFocusingTime(0F);
                }
                if (!deep_focus.isEmpty() && data.getfocustime() >= 29.41F) {
                    data.setFocusing(0);
                    data.subtractSoul(event.player, 6);
                    event.player.addEffect(new MobEffectInstance(MobEffects.HEAL, 5, 1));
                    event.player.playSound(SoundInit.FOCUS_HEAL.get(), 0.5F, 1F);
                    data.setFocusingTime(0F);
                }
                    if (deep_focus.isEmpty() && data.getfocustime() >= 22.8F) {
                        data.setFocusing(0);
                        data.subtractSoul(event.player, 6);
                        event.player.addEffect(new MobEffectInstance(MobEffects.HEAL, 5, 0));
                        event.player.playSound(SoundInit.FOCUS_HEAL.get(), 0.5F, 1F);
                        data.setFocusingTime(0F);
                    }

            }
        });
    }

    @SubscribeEvent
    public void vengefulspiritEvent(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(data ->
        {
            if(data.getSoul() >= 6 && data.checkvengefulspirit() == 1)
            {
                if (CharmAndNailKeyboardUtil.spellkey.consumeClick()) {
                    ItemStack shaman_stone =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.SHAMAN_STONE.get(), event.player).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(!shaman_stone.isEmpty()) {
                        ShamanVengefulSpiritEntity thrown = new ShamanVengefulSpiritEntity(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack spiritstack = new ItemStack(ItemInit.VENGEFUL_SPIRIT.get());
                        thrown.setItem(spiritstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.getXRot(), event.player.getYRot(), 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                       // event.player.playSound(SoundInit.SPIRIT_CAST_SOUND.get(), 0.5F, 1F);
                        event.player.level.addFreshEntity(thrown);
                        data.subtractSoul(event.player, 6);
                        System.out.println("VENGEFUL SPIRIT CAST!");
                    }
                    if(shaman_stone.isEmpty()) {
                        VengefulSpiritEntity thrown = new VengefulSpiritEntity(event.player, event.player.level);
                        //This sets the entity to the item we are holding
                        ItemStack spiritstack = new ItemStack(ItemInit.VENGEFUL_SPIRIT.get());
                        thrown.setItem(spiritstack);
                        //This is the second most important method here, it sets how the entity shall shoot.
                        // We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
                        thrown.shootFromRotation(event.player, event.player.getXRot(), event.player.getYRot(), 0.0F, 1.5F, 1F);
                        //This is the most important, it actually adds it to the world!
                      //  event.player.playSound(SoundInit.SPIRIT_CAST_SOUND.get(), 0.5F, 1F);
                        event.player.level.addFreshEntity(thrown);
                        data.subtractSoul(event.player, 6);
                        System.out.println("VENGEFUL SPIRIT CAST!");
                    }
                }

            }
        });

    }

}
