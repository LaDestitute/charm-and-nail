package com.ladestitute.charmandnail.util.events;

import com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability.CharmAndNailSoulCapability;
import com.ladestitute.charmandnail.items.ChannelledNailItem;
import com.ladestitute.charmandnail.items.OldNailItem;
import com.ladestitute.charmandnail.items.SharpenedNailItem;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import net.minecraft.core.Direction;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.List;

public class MiscCharmsEffectsHandler {
    //For later

    public double recoilrange = 0.25D;

    @SubscribeEvent
    public void rapierattacklunge(AttackEntityEvent event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlot.MAINHAND);
        if (swordstack.getItem() == ItemInit.OLD_NAIL.get()||
                swordstack.getItem() == ItemInit.SHARPENED_NAIL.get()||
                swordstack.getItem() == ItemInit.CHANNELLED_NAIL.get())
        {
            if (event.getEntityLiving().isOnGround()) {
                if (!CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    ItemStack steady_body =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), event.getPlayer()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(steady_body.isEmpty())
                    {
                        if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0,-recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0, recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.WEST) {
                            event.getEntityLiving().setDeltaMovement(recoilrange, 0, 0D);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.EAST) {
                            event.getEntityLiving().setDeltaMovement(-recoilrange, 0, 0D);
                        }
                    }
                }
            }
        }
        if (swordstack.getItem() == Items.WOODEN_SWORD||
                swordstack.getItem() == Items.STONE_SWORD||
                swordstack.getItem() == Items.GOLDEN_SWORD||
                swordstack.getItem() == Items.IRON_SWORD||
                swordstack.getItem() == Items.DIAMOND_SWORD||
                swordstack.getItem() == Items.NETHERITE_SWORD) {
            if (event.getEntityLiving().isOnGround()) {
                if (!CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    ItemStack steady_body =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), event.getPlayer()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(steady_body.isEmpty() && CharmAndNailConfig.getInstance().nailcharmseffectmelee())
                    {
                        if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0,-recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0, recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.WEST) {
                            event.getEntityLiving().setDeltaMovement(recoilrange, 0, 0D);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.EAST) {
                            event.getEntityLiving().setDeltaMovement(-recoilrange, 0, 0D);
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void rapierattacklunge2(PlayerInteractEvent.LeftClickBlock event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlot.MAINHAND);
        if (swordstack.getItem() == ItemInit.OLD_NAIL.get()||
                swordstack.getItem() == ItemInit.SHARPENED_NAIL.get()||
                swordstack.getItem() == ItemInit.CHANNELLED_NAIL.get())
        {
            if (event.getEntityLiving().isOnGround()) {
                if (!CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    ItemStack steady_body =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), event.getPlayer()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(steady_body.isEmpty())
                    {
                        if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0,-recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0, recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.WEST) {
                            event.getEntityLiving().setDeltaMovement(recoilrange, 0, 0D);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.EAST) {
                            event.getEntityLiving().setDeltaMovement(-recoilrange, 0, 0D);
                        }
                    }
                }
            }
        }
        if (swordstack.getItem() == Items.WOODEN_SWORD||
                swordstack.getItem() == Items.STONE_SWORD||
                swordstack.getItem() == Items.GOLDEN_SWORD||
                swordstack.getItem() == Items.IRON_SWORD||
                swordstack.getItem() == Items.DIAMOND_SWORD||
                swordstack.getItem() == Items.NETHERITE_SWORD) {
            if (event.getEntityLiving().isOnGround()) {
                if (!CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    ItemStack steady_body =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), event.getPlayer()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(steady_body.isEmpty() && CharmAndNailConfig.getInstance().nailcharmseffectmelee())
                    {
                        if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0,-recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0, recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.WEST) {
                            event.getEntityLiving().setDeltaMovement(recoilrange, 0, 0D);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.EAST) {
                            event.getEntityLiving().setDeltaMovement(-recoilrange, 0, 0D);
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void rapierattacklunge3(PlayerInteractEvent.LeftClickEmpty event) {
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlot.MAINHAND);
        if (swordstack.getItem() == ItemInit.OLD_NAIL.get()||
                swordstack.getItem() == ItemInit.SHARPENED_NAIL.get()||
                swordstack.getItem() == ItemInit.CHANNELLED_NAIL.get())
        {
            if (event.getEntityLiving().isOnGround()) {
                if (!CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    ItemStack steady_body =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), event.getPlayer()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(steady_body.isEmpty())
                    {
                        if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0,-recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0, recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.WEST) {
                            event.getEntityLiving().setDeltaMovement(recoilrange, 0, 0D);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.EAST) {
                            event.getEntityLiving().setDeltaMovement(-recoilrange, 0, 0D);
                        }
                    }
                }
            }
        }
        if (swordstack.getItem() == Items.WOODEN_SWORD||
                swordstack.getItem() == Items.STONE_SWORD||
                swordstack.getItem() == Items.GOLDEN_SWORD||
                swordstack.getItem() == Items.IRON_SWORD||
                swordstack.getItem() == Items.DIAMOND_SWORD||
                swordstack.getItem() == Items.NETHERITE_SWORD) {
            if (event.getEntityLiving().isOnGround()) {
                if (!CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    ItemStack steady_body =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), event.getPlayer()).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(steady_body.isEmpty() && CharmAndNailConfig.getInstance().nailcharmseffectmelee())
                    {
                        if (event.getEntityLiving().getDirection() == Direction.SOUTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0,-recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.NORTH) {
                            event.getEntityLiving().setDeltaMovement(0, 0, recoilrange);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.WEST) {
                            event.getEntityLiving().setDeltaMovement(recoilrange, 0, 0D);
                        }
                        if (event.getEntityLiving().getDirection() == Direction.EAST) {
                            event.getEntityLiving().setDeltaMovement(-recoilrange, 0, 0D);
                        }
                    }
                }
            }
        }
    }

    //Event for the spore shroom charm, which creates a 82-tick cloud that damages nearby entities
    @SubscribeEvent
    public void sporeshroomevent(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(data ->
        {
            if (data.checkifsporeshroomisactive() == 1)
            {
                data.addsporeshroomtimertick(0.5F);
                System.out.println("SPORE IS GAINING TIME: " + data.totalsporetime());
            }
            ItemStack spore_shroom =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.SPORE_SHROOM.get(), event.player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            ItemStack deep_focus =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.DEEP_FOCUS.get(), event.player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if(!spore_shroom.isEmpty()) {
                if (data.isFocusing() == 1)
                {
                    data.setsporeshroomstate(1);
                    System.out.println("SPORE IS ACTIVE");
                }
                double radius = 3;
                if(deep_focus.isEmpty()) {
                    AABB axisalignedbb = new AABB(event.player.blockPosition()).inflate(radius).expandTowards(0.0D, 16, 0.0D);
                    List<LivingEntity> list = event.player.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
                    for (LivingEntity livingEntity : list) {
                        if (!(livingEntity instanceof Player)) {
                            if (data.checksporetime() > 0 && data.checkifsporeshroomisactive() == 1) {
                                livingEntity.hurt(DamageSource.GENERIC, 1F);
                            }
                        }
                    }
                }
                if(!deep_focus.isEmpty()) {
                    AABB axisalignedbb = new AABB(event.player.blockPosition()).inflate(radius+1).expandTowards(0.0D, 16, 0.0D);
                    List<LivingEntity> list = event.player.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
                    for (LivingEntity livingEntity : list) {
                        if (!(livingEntity instanceof Player)) {
                            if (data.checksporetime() > 0 && data.checkifsporeshroomisactive() == 1) {
                                livingEntity.hurt(DamageSource.GENERIC, 1F);
                            }
                        }
                    }
                }
                if(!spore_shroom.isEmpty() && data.checksporetime() >= 4)
                {
                    data.setsporeshroomstate(0);
                    data.setsporeshroomtimertick(0);
                }
            }
        });
    }
}

