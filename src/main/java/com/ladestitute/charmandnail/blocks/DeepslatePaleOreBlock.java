package com.ladestitute.charmandnail.blocks;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;

import javax.annotation.Nullable;
import java.util.List;

public class DeepslatePaleOreBlock extends Block {

    public DeepslatePaleOreBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        p_49818_.add(new TextComponent("Rare, pale metal that emanates an icy chill. Prized by those who craft weapons."));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

}
