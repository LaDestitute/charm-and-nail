package com.ladestitute.charmandnail.items;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.List;
import java.util.UUID;

public class NotchItem extends Item {
    private static final UUID NOTCH_1 = UUID.fromString("49867573-0833-4ff5-83d5-3f168a41c923");
    private static final UUID NOTCH_2 = UUID.fromString("d6708ed7-927f-44b4-91c1-d6d3536db95d");
    private static final UUID NOTCH_3 = UUID.fromString("f6fb4f86-ac35-48fa-a104-1c75fae9aaa4");
    private static final UUID NOTCH_4 = UUID.fromString("96a8b2dc-5620-4337-8d1c-c293861d7762");
    private static final UUID NOTCH_5 = UUID.fromString("a92d2050-9cb0-4e13-86a4-cebe24ede8e9");
    private static final UUID NOTCH_6 = UUID.fromString("97fbf57e-eddb-455c-a1e7-4ddd3e71f046");
    private static final UUID NOTCH_7 = UUID.fromString("2b3278a9-1ba2-4668-82db-62cc9de2e6bc");
    private static final UUID NOTCH_8 = UUID.fromString("22e2dde2-4c4b-4f52-9113-63912f1f2374");

    public NotchItem(Item.Properties properties) {
        super(properties.stacksTo(8));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(new TextComponent("A consumable item that increases the amount of charms you can equip at once. Also fully restores your health."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        CuriosApi.getCuriosHelper().getCuriosHandler(p_41433_).ifPresent(handler -> {
            handler.getStacksHandler("charm").ifPresent(stacks -> {
                    p_41433_.setHealth(20F);
                        stacks.addPermanentModifier(new AttributeModifier(p_41433_.getUUID(), "charm", 1, AttributeModifier.Operation.ADDITION));
                        p_41433_.getMainHandItem().shrink(1);

                });
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}