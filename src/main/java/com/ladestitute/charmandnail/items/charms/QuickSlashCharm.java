package com.ladestitute.charmandnail.items.charms;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.registry.SoundInit;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.fml.common.Mod;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class QuickSlashCharm extends Item implements ICurioItem {
    private static final UUID ATTACK_SPEED = UUID.fromString("e8da52ac-8010-11ec-a8a3-0242ac120002");

    public QuickSlashCharm(Properties properties) {
        super(properties);
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(String identifier, ItemStack stack)
    {
        Multimap<Attribute, AttributeModifier> map = HashMultimap.create();
        map.put(Attributes.ATTACK_SPEED, new AttributeModifier(ATTACK_SPEED, "Curio attack speed", 0.5f, AttributeModifier.Operation.ADDITION));
        return map;
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        //Add actual charm sound from HK
        return new ICurio.SoundInfo(SoundInit.CHARM_EQUIP.get(), 1.5f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(Screen.hasShiftDown())
        {
            tooltip.add(new TextComponent("Allows the bearer to slash much more rapidly with their nail."));
        }
        else  tooltip.add(new TextComponent("Born from imperfect, discarded nails that have fused together. The nails still long to be wielded (hold Shift for more info)"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

