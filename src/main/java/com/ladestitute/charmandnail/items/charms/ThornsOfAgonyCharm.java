package com.ladestitute.charmandnail.items.charms;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.items.ChannelledNailItem;
import com.ladestitute.charmandnail.items.OldNailItem;
import com.ladestitute.charmandnail.items.SharpenedNailItem;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.registry.SoundInit;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import com.ladestitute.charmandnail.util.CharmAndNailKeyboardUtil;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.EntityDamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class ThornsOfAgonyCharm extends Item implements ICurioItem {

    public ThornsOfAgonyCharm(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void blooddamagebonus(LivingHurtEvent event)
    {
        Entity source = event.getSource().getDirectEntity();
        Entity player = event.getEntityLiving();
        ItemStack swordstack = event.getEntityLiving().getItemBySlot(EquipmentSlot.MAINHAND);
        ItemStack stack =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.THORNS_OF_AGONY.get(), (LivingEntity) player).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        if(!stack.isEmpty() && source instanceof Monster)
        {
            if(CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                    && ((LivingEntity) player).getMainHandItem().getItem() == Items.WOODEN_SWORD||
                    CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                            && ((LivingEntity) player).getMainHandItem().getItem() == Items.GOLDEN_SWORD)

            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 2);
                }
                else source.hurt(DamageSource.GENERIC, 4);
            }
            if(CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                    && swordstack.getItem() == Items.STONE_SWORD)

            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 5 / 2);
                }
                else source.hurt(DamageSource.GENERIC, 5);
            }
            if(CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                    && swordstack.getItem() == Items.IRON_SWORD)

            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 3);
                }
                else source.hurt(DamageSource.GENERIC, 6);
            }
            if(CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                    && swordstack.getItem() == Items.DIAMOND_SWORD)

            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 7 / 2);
                }
                else source.hurt(DamageSource.GENERIC, 7);
            }
            if(CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                    && swordstack.getItem() == Items.NETHERITE_SWORD)

            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 4);
                }
                else source.hurt(DamageSource.GENERIC, 8);
            }
            if(CharmAndNailConfig.getInstance().nailcharmseffectmelee()
                    && swordstack.getItem() == Items.TRIDENT)

            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 9 / 2);
                }
                else source.hurt(DamageSource.GENERIC, 9);
            }
            if(((LivingEntity) player).getMainHandItem().getItem() instanceof OldNailItem)
            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 5 / 2);
                }
                else source.hurt(DamageSource.GENERIC, 5);
            }
            if(((LivingEntity) player).getMainHandItem().getItem() instanceof SharpenedNailItem)
            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 9 / 2);
                }
                else source.hurt(DamageSource.GENERIC, 9);
            }
            if(((LivingEntity) player).getMainHandItem().getItem() instanceof ChannelledNailItem)
            {
                if(CharmAndNailConfig.getInstance().halfthornsofagonydamage()) {
                    source.hurt(DamageSource.GENERIC, 13 / 2);
                }
                else source.hurt(DamageSource.GENERIC, 13);
            }
        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        //Add actual charm sound from HK
        return new ICurio.SoundInfo(SoundInit.CHARM_EQUIP.get(), 1.5f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(Screen.hasShiftDown())
        {
            tooltip.add(new TextComponent("When taking damage, sprout thorny vines that damage nearby foes."));
        }
        else tooltip.add(new TextComponent("Senses the pain of its bearer and lashes out at the world around them (hold Shift for more info)"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

