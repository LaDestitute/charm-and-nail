package com.ladestitute.charmandnail.items.charms;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.registry.SoundInit;
import com.ladestitute.charmandnail.util.CharmAndNailKeyboardUtil;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class FragileHeartCharm extends Item implements ICurioItem {
    private static final UUID MAX_HEALTH = UUID.fromString("7e906dfa-806f-11ec-a8a3-0242ac120002");

    public FragileHeartCharm(Properties properties) {
        super(properties);
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(String identifier, ItemStack stack)
    {
        Multimap<Attribute, AttributeModifier> map = HashMultimap.create();
        map.put(Attributes.MAX_HEALTH, new AttributeModifier(MAX_HEALTH, "Curio max health", 4f, AttributeModifier.Operation.ADDITION));
        return map;
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.DESTROY;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        //Add actual charm sound from HK
        return new ICurio.SoundInfo(SoundInit.CHARM_EQUIP.get(), 1.5f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(Screen.hasShiftDown())
        {
            tooltip.add(new TextComponent("This charm is fragile, and will break if its bearer is killed."));
        }
        else tooltip.add(new TextComponent("Increases the max health of the bearer, allowing them to take more damage (hold Shift for more info)"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}


