package com.ladestitute.charmandnail.items.charms;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.items.ChannelledNailItem;
import com.ladestitute.charmandnail.items.OldNailItem;
import com.ladestitute.charmandnail.items.SharpenedNailItem;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.registry.SoundInit;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import com.ladestitute.charmandnail.util.CharmAndNailKeyboardUtil;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingKnockBackEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class SteadyBodyCharm extends Item implements ICurioItem {

    public SteadyBodyCharm(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void livingDamageEvent(LivingKnockBackEvent event) {
        //Check if is player doing the damage.
        if (event.getEntity() instanceof Player) {

            //Get Player.
            Player player = (Player) event.getEntity();

            //Get the Ring as an ItemStack
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STEADY_BODY.get(), player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

            //Check if player is wearing it. Check if Sword Item.

                if (!stack.isEmpty() && CharmAndNailConfig.getInstance().steadybodypreventsmobknockback()) {
                    event.setStrength(0);
                }

        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        //Add actual charm sound from HK
        return new ICurio.SoundInfo(SoundInit.CHARM_EQUIP.get(), 1.5f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(Screen.hasShiftDown() && CharmAndNailConfig.getInstance().steadybodypreventsmobknockback())
        {
            tooltip.add(new TextComponent("Allows one to become immune to knockback"));
        }
        if(Screen.hasShiftDown() && !CharmAndNailConfig.getInstance().steadybodypreventsmobknockback())
        {
            tooltip.add(new TextComponent("Allows one to stay steady and keep attacking without being pushed back by their own blade"));
        }
        else  tooltip.add(new TextComponent("Keeps its bearer from recoiling backwards when they are struck by an enemy (hold Shift for more info)"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

