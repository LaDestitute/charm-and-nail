package com.ladestitute.charmandnail.items.charms;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.items.ChannelledNailItem;
import com.ladestitute.charmandnail.items.OldNailItem;
import com.ladestitute.charmandnail.items.SharpenedNailItem;
import com.ladestitute.charmandnail.registry.ItemInit;
import com.ladestitute.charmandnail.registry.SoundInit;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class FuryOfTheFallenCharm extends Item implements ICurioItem {

    public FuryOfTheFallenCharm(Properties properties) {
        super(properties);
    }

    @SubscribeEvent
    public static void livingDamageEvent(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof Player) {

            //Get Player.
            Player player = (Player) event.getSource().getDirectEntity();

            //Get the Ring as an ItemStack
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FURY_OF_THE_FALLEN.get(), player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

            //Check if player is wearing it. Check if Sword Item.
            if(player.getHealth() <= 2) {
                if(CharmAndNailConfig.getInstance().nailcharmseffectmelee())
                {
                    if (!stack.isEmpty() && player.getMainHandItem().getItem() instanceof SwordItem||
                            !stack.isEmpty() && player.getMainHandItem().getItem() instanceof TridentItem)
                    {
                        event.setAmount(event.getAmount() * 1.75f);
                    }
                }
                if (!stack.isEmpty() && player.getMainHandItem().getItem() instanceof OldNailItem ||
                        !stack.isEmpty() && player.getMainHandItem().getItem() instanceof SharpenedNailItem ||
                        !stack.isEmpty() && player.getMainHandItem().getItem() instanceof ChannelledNailItem) {
                    event.setAmount(event.getAmount() * 1.75f);
                }
            }
        }
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        //Add actual charm sound from HK
        return new ICurio.SoundInfo(SoundInit.CHARM_EQUIP.get(), 1.5f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(Screen.hasShiftDown())
        {
            tooltip.add(new TextComponent("When close to death, the bearer's strength will increase."));
        }
        else  tooltip.add(new TextComponent("Embodies the fury and heroism that comes upon those who are about to die (hold Shift for more info)"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
