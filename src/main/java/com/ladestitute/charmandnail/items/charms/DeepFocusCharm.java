package com.ladestitute.charmandnail.items.charms;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.registry.SoundInit;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.fml.common.Mod;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class DeepFocusCharm extends Item implements ICurioItem {

    public DeepFocusCharm(Properties properties) {
        super(properties);
    }

    @Deprecated
    public ICurio.DropRule getDropRule(LivingEntity livingEntity) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }

    @Deprecated
    public ICurio.SoundInfo getEquipSound(SlotContext slotContext) {
        //Add actual charm sound from HK
        return new ICurio.SoundInfo(SoundInit.CHARM_EQUIP.get(), 1.5f, 1.0f);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(Screen.hasShiftDown())
        {
            tooltip.add(new TextComponent("The bearer will focus SOUL at a slower rate, but the healing effect will double."));
        }
        else  tooltip.add(new TextComponent(
                "Naturally formed within a crystal over a long period. Draws in SOUL from the surrounding air (hold Shift for more info)"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
