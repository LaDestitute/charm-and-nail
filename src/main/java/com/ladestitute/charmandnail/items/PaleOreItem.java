package com.ladestitute.charmandnail.items;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class PaleOreItem extends Item {
    public PaleOreItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(new TextComponent("Rare, pale metal that emanates an icy chill. Prized by those who craft weapons."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
