package com.ladestitute.charmandnail.items.spells.unlocks;

import com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability.CharmAndNailSoulCapability;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class VengefulSpiritSpellItem extends Item {
    public VengefulSpiritSpellItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(new TextComponent("Use to unlock Vengeful Spirit, conjure a spirit that will fly forward and burn foes in its path."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        p_41433_.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(data ->
        {
            if(data.checkvengefulspirit() == 0)
            {
                data.setvengefulspirit(1);
                p_41433_.getMainHandItem().shrink(1);
            }
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}

