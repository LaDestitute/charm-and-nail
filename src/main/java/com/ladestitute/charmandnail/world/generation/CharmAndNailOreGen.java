package com.ladestitute.charmandnail.world.generation;

import java.util.ArrayList;
import java.util.List;

import com.ladestitute.charmandnail.registry.BlockInit;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.features.OreFeatures;
import net.minecraft.data.worldgen.placement.OrePlacements;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTest;
import java.util.ArrayList;
import java.util.List;

public class CharmAndNailOreGen {
    public static final List<PlacedFeature> OVERWORLD_ORES = new ArrayList<>();

    public static void registerOres() {
        final ConfiguredFeature<?, ?> paleDeepslateOre = FeatureUtils.register("deepslate_pale_ore",
                Feature.ORE.configured(new OreConfiguration(List.of(
                        OreConfiguration.target(OreFeatures.DEEPSLATE_ORE_REPLACEABLES,
                                BlockInit.DEEPSLATE_PALE_ORE.get().defaultBlockState())),
                        3)));

        final PlacedFeature placedeepslatePaleOre = PlacementUtils.register("deepslate_pale_ore",
                paleDeepslateOre.placed(OrePlacements.commonOrePlacement(3, HeightRangePlacement
                        .uniform(VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(16)))));
     //   OVERWORLD_ORES.add(placedPaleOre);
        OVERWORLD_ORES.add(placedeepslatePaleOre);
    }
}
