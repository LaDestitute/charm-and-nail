package com.ladestitute.charmandnail.world.generation;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.util.enums.CharmAndNailToolMaterials;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID)
public class CharmAndNailWorldGen {

        @SubscribeEvent
        public static void onBiomeLoad(BiomeLoadingEvent event) {
            final List<Supplier<PlacedFeature>> features = event.getGeneration()
                    .getFeatures(GenerationStep.Decoration.UNDERGROUND_ORES);
            CharmAndNailOreGen.OVERWORLD_ORES.forEach(ore -> features.add(() -> ore));

            if (event.getCategory() == Biome.BiomeCategory.PLAINS){
                List<MobSpawnSettings.SpawnerData> spawns =
                        event.getSpawns().getSpawner(MobCategory.CREATURE);
               // spawns.add(new MobSpawnSettings.SpawnerData(EntityType.SKELETON_HORSE, 4, 2, 3));

            }

        }
    }
