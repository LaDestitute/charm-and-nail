package com.ladestitute.charmandnail.network;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.network.client.ClientboundPlayerSoulUpdateMessage;
import com.ladestitute.charmandnail.network.server.ExtendedAttackPacket;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.simple.SimpleChannel;

import java.util.function.Function;

public final class SimpleNetworkHandler {
    //This should be 1.0
    public static final String NETWORK_VERSION = "1.0";

    //The channel the packets are sent in
    //It takes the name of a modid, the channel name, a protocol version supplier (in short, sides),
    //server and client accepted versions are predicates, basically the protocol versions they will accept
    public static final SimpleChannel CHANNEL = NetworkRegistry.ChannelBuilder.named(
            new ResourceLocation(CharmAndNailMain.MOD_ID, "main")).networkProtocolVersion(() ->
            NETWORK_VERSION).serverAcceptedVersions(NETWORK_VERSION::equals).clientAcceptedVersions(
            NETWORK_VERSION::equals).simpleChannel();

    private static int index;

    //An init where we register our packets
    public static void init() {
        //This type of packet is server to client
        registerMessage(index++, ClientboundPlayerSoulUpdateMessage.class, ClientboundPlayerSoulUpdateMessage::new);
        register(ExtendedAttackPacket.class, ExtendedAttackPacket::decode);
    }

    private static <MSG extends ICaNPacket.CaNPacket> void register(final Class<MSG> packet, Function<FriendlyByteBuf, MSG> decoder) {
        CHANNEL.messageBuilder(packet, index++).encoder(ICaNPacket.CaNPacket::encode).decoder(decoder).consumer(ICaNPacket.CaNPacket::handle).add();
    }


    public static <MSG> void sendToServer(MSG message)
    {
        CHANNEL.sendToServer(message);
    }


    private static <T extends INormalMessage> void registerMessage(int index, Class<T> messageType, Function<FriendlyByteBuf, T> decoder) {
        //Encoding is saving information to the byte buffer, decoding is the opposite of that (reading info)
        CHANNEL.registerMessage(index, messageType, INormalMessage::toBytes, decoder, (message, context) -> {
            message.process(context);
            context.get().setPacketHandled(true);
        });
    }
}