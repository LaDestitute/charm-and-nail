package com.ladestitute.charmandnail.network.client;

import com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability.CharmAndNailSoulCapability;
import com.ladestitute.charmandnail.network.INormalMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientboundPlayerSoulUpdateMessage implements INormalMessage {
    int Soul;

    public ClientboundPlayerSoulUpdateMessage(int soul) {
        this.Soul = soul;
    }

    public ClientboundPlayerSoulUpdateMessage(FriendlyByteBuf buf) {
        Soul = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(Soul);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setSoul(Soul);
                    }));
        }
    }
}

