package com.ladestitute.charmandnail.client;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.registry.EntityTypeInit;
import com.ladestitute.charmandnail.util.CharmAndNailKeyboardUtil;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
    }

    public static void init(final FMLClientSetupEvent event) {
        CharmAndNailKeyboardUtil.register();
    }

    public static void initRenders(EntityRenderersEvent.RegisterRenderers event) {
        event.registerEntityRenderer(EntityTypeInit.VENGEFUL_SPIRIT.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.SHAMAN_VENGEFUL_SPIRIT.get(),
                ThrownItemRenderer::new);
    }


}
