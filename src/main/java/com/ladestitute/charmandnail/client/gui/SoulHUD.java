package com.ladestitute.charmandnail.client.gui;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability.CharmAndNailSoulCapability;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = CharmAndNailMain.MOD_ID)
public class SoulHUD extends GuiComponent {
    public final static ResourceLocation SOUL = new ResourceLocation(CharmAndNailMain.MOD_ID, "textures/gui/soul.png");
    protected final static int WIDTH = 9;
    protected final static int HEIGHT = 9;

    protected Minecraft mc;

    public SoulHUD(Minecraft mc) {
        this.mc = mc;

    }

    public void render(PoseStack poseStack, int screenWidth, int screenHeight) {
        poseStack.pushPose();
        RenderSystem.enableBlend();
        RenderSystem.setShaderTexture(0, SOUL);
        mc.player.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(info -> {
            int soul = info.getSoul();
            if(CharmAndNailConfig.getInstance().soulmeterposition() == 1)
            {
                int OffsetX = screenWidth / 2 - 130;
                int OffsetY = screenHeight - 235;

                int texU = 0;
                int texV = 0;
                // info.getmaxSoul()/2 is done instead of "10" so it can scale to soul above 20
                for (int i = 0; i < info.getmaxSoul() / 2; ++i) {
                    int OffsetX1 = OffsetX - i * 8 - 1;
                    this.blit(poseStack, OffsetX1, OffsetY, 36 + texU, texV, WIDTH, HEIGHT);

                    if (i * 2 + 1 < soul) {
                        this.blit(poseStack, OffsetX1, OffsetY, texU, texV, WIDTH, HEIGHT);
                    }
                    if (i * 2 + 1 == soul) {
                        this.blit(poseStack, OffsetX1, OffsetY, texU + 9, texV, WIDTH, HEIGHT);
                    }


                }
            }
            if(CharmAndNailConfig.getInstance().soulmeterposition() == 2)
            {
                int OffsetX = screenWidth / 2 + 35;
                int OffsetY = screenHeight - 235;

                int texU = 0;
                int texV = 0;
                // info.getmaxSoul()/2 is done instead of "10" so it can scale to soul above 20
                for (int i = 0; i < info.getmaxSoul() / 2; ++i) {
                    int OffsetX1 = OffsetX - i * 8 - 1;
                    this.blit(poseStack, OffsetX1, OffsetY, 36 + texU, texV, WIDTH, HEIGHT);

                    if (i * 2 + 1 < soul) {
                        this.blit(poseStack, OffsetX1, OffsetY, texU, texV, WIDTH, HEIGHT);
                    }
                    if (i * 2 + 1 == soul) {
                        this.blit(poseStack, OffsetX1, OffsetY, texU + 9, texV, WIDTH, HEIGHT);
                    }


                }
            }
            if(CharmAndNailConfig.getInstance().soulmeterposition() == 3)
            {
                int OffsetX = screenWidth / 2 + 195;
                int OffsetY = screenHeight - 235;

                int texU = 0;
                int texV = 0;
                // info.getmaxSoul()/2 is done instead of "10" so it can scale to soul above 20
                for (int i = 0; i < info.getmaxSoul() / 2; ++i) {
                    int OffsetX1 = OffsetX - i * 8 - 1;
                    this.blit(poseStack, OffsetX1, OffsetY, 36 + texU, texV, WIDTH, HEIGHT);

                    if (i * 2 + 1 < soul) {
                        this.blit(poseStack, OffsetX1, OffsetY, texU, texV, WIDTH, HEIGHT);
                    }
                    if (i * 2 + 1 == soul) {
                        this.blit(poseStack, OffsetX1, OffsetY, texU + 9, texV, WIDTH, HEIGHT);
                    }


                }
            }
        });
        poseStack.popPose();
    }
}

