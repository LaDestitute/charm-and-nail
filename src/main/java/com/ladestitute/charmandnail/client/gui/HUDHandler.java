package com.ladestitute.charmandnail.client.gui;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability.CharmAndNailSoulCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = CharmAndNailMain.MOD_ID)
public class HUDHandler {
    private final static SoulHUD SOUL_HUD = new SoulHUD(Minecraft.getInstance());

    @SubscribeEvent(receiveCanceled = true)
    public static void onRenderGameOverlayEvent(RenderGameOverlayEvent.Post event) {
        int screenHeight = event.getWindow().getGuiScaledHeight();
        int screenWidth = event.getWindow().getGuiScaledWidth();
        LocalPlayer playerEntity = Minecraft.getInstance().player;
        //render soul
        if (event.getType() == RenderGameOverlayEvent.ElementType.LAYER) {
            //   if (playerEntity != null && !playerEntity.isCreative() && !playerEntity.isSpectator() && !mc.options.hideGui) {
            //     playerEntity.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(cap -> {
            //         SOUL_HUD.render(event.getMatrixStack(), screenWidth, screenHeight);
            //      });
            //   }
            //For testing purposes, comment/remove this and use the above one
            if (playerEntity != null) {
                playerEntity.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(cap -> {
                    SOUL_HUD.render(event.getMatrixStack(), screenWidth, screenHeight);
                });
            }
        }

    }
}

