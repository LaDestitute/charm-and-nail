package com.ladestitute.charmandnail.registry;

import com.ladestitute.charmandnail.CharmAndNailMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class SoundInit {
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, CharmAndNailMain.MOD_ID);

    public static final RegistryObject<SoundEvent> NAIL_SOUND = SOUNDS.register("entity.melee.nail_hit",
            () -> new SoundEvent(new ResourceLocation(CharmAndNailMain.MOD_ID, "entity.melee.nail_hit")));

    public static final RegistryObject<SoundEvent> SPIRIT_CAST_SOUND = SOUNDS.register("entity.projectiles.vengeful_spirit",
            () -> new SoundEvent(new ResourceLocation(CharmAndNailMain.MOD_ID, "entity.projectiles.vengeful_spirit")));

    public static final RegistryObject<SoundEvent> FOCUS_HEAL = SOUNDS.register("entity.focus_heal",
            () -> new SoundEvent(new ResourceLocation(CharmAndNailMain.MOD_ID, "entity.focus_heal")));

    public static final RegistryObject<SoundEvent> CHARM_EQUIP = SOUNDS.register("entity.other.charm_click",
            () -> new SoundEvent(new ResourceLocation(CharmAndNailMain.MOD_ID, "entity.other.charm_click")));
}
