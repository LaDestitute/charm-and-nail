package com.ladestitute.charmandnail.registry;

import com.ladestitute.charmandnail.CharmAndNailMain;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SpecialBlockInit {
    //A class for blocks we don't want automatically registered into our creative tab and/or put into a different one
    public static final DeferredRegister<Block> SPECIAL_BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            CharmAndNailMain.MOD_ID);
}
