package com.ladestitute.charmandnail.registry;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.blocks.DeepslatePaleOreBlock;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            CharmAndNailMain.MOD_ID);

  //  public static final RegistryObject<Block> PALE_ORE = BLOCKS.register("pale_ore",
    //        () -> new PaleOreBlock(PropertiesInit.PALE_ORE));
    public static final RegistryObject<Block> DEEPSLATE_PALE_ORE = BLOCKS.register("deepslate_pale_ore",
            () -> new DeepslatePaleOreBlock(PropertiesInit.DEEPSLATE_PALE_ORE));
}
