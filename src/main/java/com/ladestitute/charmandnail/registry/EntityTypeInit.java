package com.ladestitute.charmandnail.registry;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.entities.spells.ShamanVengefulSpiritEntity;
import com.ladestitute.charmandnail.entities.spells.VengefulSpiritEntity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class EntityTypeInit {
    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, CharmAndNailMain.MOD_ID);
    //Projectiles
    public static final RegistryObject<EntityType<VengefulSpiritEntity>> VENGEFUL_SPIRIT = ENTITIES.register("vengeful_spirit",
            () -> EntityType.Builder.<VengefulSpiritEntity>of(VengefulSpiritEntity::new, MobCategory.MISC)
                    .sized(0.25F, 0.25F).build("vengeful_spirit"));
    public static final RegistryObject<EntityType<ShamanVengefulSpiritEntity>> SHAMAN_VENGEFUL_SPIRIT = ENTITIES.register("shaman_vengeful_spirit",
            () -> EntityType.Builder.<ShamanVengefulSpiritEntity>of(ShamanVengefulSpiritEntity::new, MobCategory.MISC)
                    .sized(0.25F, 0.25F).build("shaman_vengeful_spirit"));

}
