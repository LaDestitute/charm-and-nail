package com.ladestitute.charmandnail.registry;

import com.ladestitute.charmandnail.CharmAndNailMain;
import com.ladestitute.charmandnail.items.NotchItem;
import com.ladestitute.charmandnail.items.OldNailItem;
import com.ladestitute.charmandnail.items.PaleOreItem;
import com.ladestitute.charmandnail.items.SharpenedNailItem;
import com.ladestitute.charmandnail.items.charms.*;
import com.ladestitute.charmandnail.items.spells.unlocks.VengefulSpiritSpellItem;
import com.ladestitute.charmandnail.util.enums.CharmAndNailToolMaterials;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            CharmAndNailMain.MOD_ID);

    public static final RegistryObject<Item> PALE_ORE_ITEM = ITEMS.register("pale_ore_item",
            () -> new PaleOreItem(new Item.Properties().tab(CharmAndNailMain.MISC_TAB)));

    public static final RegistryObject<Item> OLD_NAIL = ITEMS.register("old_nail",
            () -> new OldNailItem(CharmAndNailToolMaterials.OLD_NAIL, 0, 0f,
                    new Item.Properties().tab(CharmAndNailMain.NAILS_TAB)));

    public static final RegistryObject<Item> SHARPENED_NAIL = ITEMS.register("sharpened_nail",
            () -> new SharpenedNailItem(CharmAndNailToolMaterials.SHARPENED_NAIL, 2, 0f,
                    new Item.Properties().tab(CharmAndNailMain.NAILS_TAB)));

    public static final RegistryObject<Item> CHANNELLED_NAIL = ITEMS.register("channelled_nail",
            () -> new SharpenedNailItem(CharmAndNailToolMaterials.CHANNELLED_NAIL, 4, 0f,
                    new Item.Properties().tab(CharmAndNailMain.NAILS_TAB)));

    //Spell unlocks + spell projectiles and entities
    public static final RegistryObject<Item> VENGEFUL_SPIRIT = ITEMS.register("vengeful_spirit",
            () -> new ThornsOfAgonyCharm(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> VENGEFUL_SPIRIT_SPELL = ITEMS.register("vengeful_spirit_spell",
            () -> new VengefulSpiritSpellItem(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.MISC_TAB)));

    //Charms
    public static final RegistryObject<Item> DEEP_FOCUS = ITEMS.register("deep_focus",
            () -> new DeepFocusCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> FRAGILE_HEART = ITEMS.register("fragile_heart",
            () -> new FragileHeartCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> FRAGILE_STRENGTH = ITEMS.register("fragile_strength",
            () -> new FragileStrengthCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> FURY_OF_THE_FALLEN = ITEMS.register("fury_of_the_fallen",
            () -> new FuryOfTheFallenCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> GRUBSONG = ITEMS.register("grubsong",
            () -> new GrubsongCharm(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> HEAVY_BLOW = ITEMS.register("heavy_blow",
            () -> new HeavyBlowCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> LONGNAIL = ITEMS.register("longnail",
            () -> new LongnailCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> QUICK_FOCUS = ITEMS.register("quick_focus",
            () -> new QuickFocusCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> QUICK_SLASH = ITEMS.register("quick_slash",
            () -> new QuickSlashCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> SHAMAN_STONE = ITEMS.register("shaman_stone",
            () -> new ShamanStoneCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> SOUL_CATCHER = ITEMS.register("soul_catcher",
            () -> new SoulCatcherCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> SPORE_SHROOM = ITEMS.register("spore_shroom",
            () -> new SporeShroomCharm(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> STEADY_BODY = ITEMS.register("steady_body",
            () -> new SteadyBodyCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));
    public static final RegistryObject<Item> THORNS_OF_AGONY = ITEMS.register("thorns_of_agony",
            () -> new ThornsOfAgonyCharm(new Item.Properties().stacksTo(1).tab(CharmAndNailMain.CHARMS_TAB)));

    public static final RegistryObject<Item> CHARM_NOTCH_ITEM = ITEMS.register("charm_notch",
            () -> new NotchItem(new Item.Properties()));


}
