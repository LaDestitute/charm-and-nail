package com.ladestitute.charmandnail.registry;

import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;

public class PropertiesInit {
    //A helper class for storing block properties in a dedicated class like how some mods used to use a Reference-class for modid and more
    //You can always skip using this and just list properties directly in registering but I feel this makes block registering less cluttered
    //First parameter in .strength is hardness, second is blast resistance
    public static final BlockBehaviour.Properties PALE_ORE = BlockBehaviour.Properties.of(Material.STONE)
            .strength(3f, 3f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties DEEPSLATE_PALE_ORE = BlockBehaviour.Properties.of(Material.STONE)
            .strength(4.5f, 3f)
            .sound(SoundType.DEEPSLATE)
            .requiresCorrectToolForDrops();
}
