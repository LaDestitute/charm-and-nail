package com.ladestitute.charmandnail;

import com.ladestitute.charmandnail.client.ClientEventBusSubscriber;
import com.ladestitute.charmandnail.network.SimpleNetworkHandler;
import com.ladestitute.charmandnail.registry.*;
import com.ladestitute.charmandnail.util.CharmAndNailConfig;
import com.ladestitute.charmandnail.util.datagen.CharmAndNailDataGen;
import com.ladestitute.charmandnail.util.events.MiscCharmsEffectsHandler;
import com.ladestitute.charmandnail.util.events.SoulEffectsHandler;
import com.ladestitute.charmandnail.util.glm.CharmAndNailDropModifier;
import com.ladestitute.charmandnail.world.generation.CharmAndNailOreGen;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.item.*;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotTypeMessage;

@Mod(CharmAndNailMain.MOD_ID)
@EventBusSubscriber(modid = CharmAndNailMain.MOD_ID, bus = Bus.MOD)
public class CharmAndNailMain
{
    //To make use of this template, just change the "modid"s in the build.gradle and mod.toml
    //Don't forget to rename the asset folders to your modid as well
    //Don't forget to refactor-rename packages or classes if desired
    //Also check resources, it has some example jsons you can copy/refer to
    //As well as the newer data-driven way of handling block tool requirements from +1.17 onward:
    // https://gist.github.com/gigaherz/691f528a61f631af90c9426c076a298a
    //This set of templates also has an access transformer already in
    //With some at-lines for structures in case you decide to implement some, with the AT-part already done.
    public static CharmAndNailMain instance;
    public static final String MOD_NAME = "Charm and Nail";
    public static final String MOD_ID = "charmandnail";
    public static final String NETWORK_VERSION = "1.0";
    public static final Logger LOGGER = LogManager.getLogger();

    public CharmAndNailMain()
    {
        instance = this;
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, CharmAndNailConfig.SPEC, "charmandnailmodconfig.toml");
        final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        //Uncomment if you are adding structures (TelepathicGrunt has an example-github on how)
        //Structures.DEFERRED_REGISTRY_STRUCTURE.register(modEventBus);
        modEventBus.addListener(this::setup);

        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
        {
         //   MinecraftForge.EVENT_BUS.register(new RenderSoulMeterHandler());
            //For stuff that should only be run client-side, such as a music ticker
           //MinecraftForge.EVENT_BUS.register(MusicHandler.class);
        });
        //Example mod event bus
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientEventBusSubscriber::init);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientEventBusSubscriber::initRenders);
        MinecraftForge.EVENT_BUS.register(new MiscCharmsEffectsHandler());
        MinecraftForge.EVENT_BUS.register(new SoulEffectsHandler());
        MinecraftForge.EVENT_BUS.addListener(this::villagerTrades);
        /* Register all of our deferred registries from our list/init classes, which get added to the IEventBus */
        //Some example registries to setup a basic mod
        CharmAndNailDropModifier.GLM.register(modEventBus);
        CharmAndNailDataGen.GLM.register(modEventBus);
        SoundInit.SOUNDS.register(modEventBus);
        ItemInit.ITEMS.register(modEventBus);
        SpecialBlockInit.SPECIAL_BLOCKS.register(modEventBus);
        BlockInit.BLOCKS.register(modEventBus);
        EntityTypeInit.ENTITIES.register(modEventBus);

    }

    //Helper method for setting the shape of custom-model blocks
    public static VoxelShape calculateShapes(Direction to, VoxelShape shape) {
        final VoxelShape[] buffer = { shape, Shapes.empty() };

        final int times = (to.get2DDataValue() - Direction.NORTH.get2DDataValue() + 4) % 4;
        for (int i = 0; i < times; i++) {
            buffer[0].forAllBoxes((minX, minY, minZ, maxX, maxY, maxZ) -> buffer[1] = Shapes.or(buffer[1],
                    Shapes.create(1 - maxZ, minY, minX, 1 - minZ, maxY, maxX)));
            buffer[0] = buffer[1];
            buffer[1] = Shapes.empty();
        }

        return buffer[0];
    }

    //Helper method to automatically register blocks and their item-forms
    @SubscribeEvent
    public static void createBlockItems(final RegistryEvent.Register<Item> event) {
        final IForgeRegistry<Item> registry = event.getRegistry();

        BlockInit.BLOCKS.getEntries().stream().map(RegistryObject::get).forEach(block -> {
            final Item.Properties properties = new Item.Properties().tab(CharmAndNailMain.BLOCKS_TAB);
            final BlockItem blockItem = new BlockItem(block, properties);
            blockItem.setRegistryName(block.getRegistryName());
            registry.register(blockItem);
        });
    }

    /* The FMLCommonSetupEvent (FML - Forge Mod Loader) */
    private void setup(final FMLCommonSetupEvent event)
    {
        //Uncomment if you are adding structures (TelepathicGrunt has an example-github on how)
        // BTDStructures.setupStructures();
        // BTDConfiguredStructures.registerConfiguredStructures();
        event.enqueueWork(CharmAndNailOreGen::registerOres);
        event.enqueueWork(SimpleNetworkHandler::init);
    }

    private void villagerTrades(VillagerTradesEvent event)
    {
        if(event.getType() == VillagerProfession.WEAPONSMITH)
        {
            event.getTrades().get(1).add(CharmAndNailMain.buildTrade(new ItemStack(Items.EMERALD, 5),
                    new ItemStack(ItemInit.OLD_NAIL.get(), 1), new ItemStack(ItemInit.SHARPENED_NAIL.get(), 1),
                    100, 10, 0F));

        }


    }

    public static VillagerTrades.ItemListing buildTrade(ItemStack wanted1, ItemStack given, int tradesUntilDisabled, int xpToVillagr, float priceMultiplier)
    {
        return CharmAndNailMain.buildTrade(wanted1, ItemStack.EMPTY, given, tradesUntilDisabled, xpToVillagr, priceMultiplier);
    }

    public static VillagerTrades.ItemListing buildTrade(ItemStack wanted1, ItemStack wanted2, ItemStack given, int tradesUntilDisabled, int xpToVillagr, float priceMultiplier)
    {
        return (entity, random) -> new MerchantOffer(wanted1, wanted2, given, tradesUntilDisabled, xpToVillagr, priceMultiplier);
    }

    @SubscribeEvent
    public static void interModComms(InterModEnqueueEvent e){
        InterModComms.sendTo(CuriosApi.MODID, SlotTypeMessage.REGISTER_TYPE, () -> new SlotTypeMessage.Builder("charm")
                .size(3).icon(new ResourceLocation("curios:slot/charm_notch")).build());
    }

    // Custom CreativeModeTab tab
    public static final CreativeModeTab BLOCKS_TAB = new CreativeModeTab("charmandnail_blocks") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(BlockInit.DEEPSLATE_PALE_ORE.get());
        }
    };

    public static final CreativeModeTab NAILS_TAB = new CreativeModeTab("charmandnail_nails") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.OLD_NAIL.get());
        }
    };

    public static final CreativeModeTab CHARMS_TAB = new CreativeModeTab("charmandnail_charms") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.SOUL_CATCHER.get());
        }
    };

    public static final CreativeModeTab MISC_TAB = new CreativeModeTab("charmandnail_misc") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.PALE_ORE_ITEM.get());
        }
    };

}
