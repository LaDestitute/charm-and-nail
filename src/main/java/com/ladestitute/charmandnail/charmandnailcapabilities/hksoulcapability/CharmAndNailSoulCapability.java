package com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability;

import com.ladestitute.charmandnail.network.client.ClientboundPlayerSoulUpdateMessage;
import com.ladestitute.charmandnail.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CharmAndNailSoulCapability {

    //Using a float makes the meter behave strangely, use ints instead if you copy the example
    //As expected with ints, you'll have to use whole numbers, 1 for half-sphere, 2 for a whole
    //I.e, 6 translates to three spheres, 5 translates to two and a half spheres
    //Visually, going over 20 will probably act as a 'reserve', GUIs will only deplete when they hit 20 or below
    private int maxSoul = 20;
    private int currentSoul = 0;
    private int isfocusing = 0;
    private float focustime = 0F;
    private int haslearnedvengefulspirit = 0;
    private float sporeshroomtimer = 0F;
    private int sporeshroomcloudactive = 0;
    private int isplayerstandingstill = 0;

    public void addSoul(Player player, int amount) {
        if (this.currentSoul < this.maxSoul) {
            this.currentSoul += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentSoul = this.maxSoul;
        soulClientUpdate(player);
    }

    public void subtractSoul(Player player, int amount) {
        if (this.currentSoul > 0) {
            this.currentSoul -= amount;
        }
        //Failsafe to prevent underflowing
        //There may be a small chance it will underflow to -1 but
        //It will always usually self-correct to 0 next update if whenever
        else this.currentSoul = 0;
        soulClientUpdate(player);
    }

    //This is the only method where you may need to manually sync like the example commented out in the eventhandler
    public void setSoul(int amount) {
        this.currentSoul = amount;
    }

    public void setMaxSoul(int amount) {
        this.maxSoul = amount;
    }

    public void setFocusing(int amount) {
        this.isfocusing = amount;
    }

    public void setFocusingTime(float amount) {
        this.focustime = amount;
    }

    public int isFocusing()
    {
        return isfocusing;
    }

    public float totalsporetime()
    {
        return sporeshroomtimer;
    }

    public float getfocustime()
    {
        return focustime;
    }

    public int getisstandingstill() {return isplayerstandingstill; }

    public void addFocusingTime(float amount) {
        this.focustime += amount;
    }

    public void setstandingstill(int amount) {this.isplayerstandingstill = amount; }

    public int getSoul() {
        return currentSoul;
    }

    public int getmaxSoul() {
        return maxSoul;
    }

    public int checkvengefulspirit()
    {
        return haslearnedvengefulspirit;
    }

    public float checksporetime() { return sporeshroomtimer; }

    public int checkifsporeshroomisactive() {return sporeshroomcloudactive; }

    public void setvengefulspirit(int amount)
    {
        this.haslearnedvengefulspirit = amount;
    }

    public void addsporeshroomtimertick(float amount)
    {
        this.sporeshroomtimer += amount;
    }

    public void setsporeshroomtimertick(float amount)
    {
        this.sporeshroomtimer = amount;
    }

    public void setsporeshroomstate(int amount) {
        this.sporeshroomcloudactive = amount;
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void soulClientUpdate(Player player) {
        if (!player.level.isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(Provider.PLAYER_SOUL).ifPresent(cap ->
                    SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientboundPlayerSoulUpdateMessage(cap.currentSoul)));
        }
    }

    public static Tag writeNBT(Capability<CharmAndNailSoulCapability> capability, CharmAndNailSoulCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentsoul", instance.getSoul());
        tag.putInt("vengefulspirit", instance.haslearnedvengefulspirit);
        return tag;
    }

    public static void readNBT(Capability<CharmAndNailSoulCapability> capability, CharmAndNailSoulCapability instance, Direction side, Tag nbt) {
        instance.setSoul(((CompoundTag) nbt).getInt("currentsoul"));
        instance.setvengefulspirit(((CompoundTag) nbt).getInt("vengefulspirit"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<CharmAndNailSoulCapability> PLAYER_SOUL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final CharmAndNailSoulCapability instance;

        private final LazyOptional<CharmAndNailSoulCapability> handler;

        public Provider() {
            instance = new CharmAndNailSoulCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return PLAYER_SOUL.orEmpty(cap, handler);
        }

        public void invalidate() {
            handler.invalidate();
        }


        public CharmAndNailSoulCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return CharmAndNailSoulCapability.writeNBT(PLAYER_SOUL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            CharmAndNailSoulCapability.readNBT(PLAYER_SOUL, instance, null, nbt);
        }
    }

}
