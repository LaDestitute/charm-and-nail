package com.ladestitute.charmandnail.charmandnailcapabilities.hksoulcapability;

import com.ladestitute.charmandnail.CharmAndNailMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = CharmAndNailMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class CharmAndNailSoulCapabilityHandler {

    //Make sure this isn't invalid, if it, it will cause a crash
    //We provide a mod ID so it can be recognized with a specified path for the factory class
    public static final ResourceLocation CHARMANDNAILSOUL_CAP = new ResourceLocation(CharmAndNailMain.MOD_ID, "charmandnailsoulcapabilityfactory");

    //As explained below, this event is responsible for attaching and also removing the capability if the entity/item/etc no longer exists
    @SubscribeEvent
    public static void attachCapability(AttachCapabilitiesEvent<Entity> event) {
        //Do nothing if the entity is not the player
        if (event.getObject() instanceof Player && event.getObject().isAlive()) {
            //We attach our capability here to the player, another entity, a chunk, blocks, items, etc
            CharmAndNailSoulCapability.Provider provider = new CharmAndNailSoulCapability.Provider();
            event.addCapability(CHARMANDNAILSOUL_CAP, new CharmAndNailSoulCapability.Provider());
            event.addListener(provider::invalidate);
            //  System.out.println("CAPABILITY ATTACHED TO PLAYER");
            //The invalidate listener is a listener that removes the capability when the entity/itemstack/etc is destroyed, so we can clear our lazyoptional
            //This is important, if something else gets our cap and stores the value, it becomes invalid and is not usable for safety reasons
            //Otherwise, this creates a dangling reference, which is a reference to an object that no longer exists
            //As a result, make sure not to have the invalidate line missing when attaching a capability
            //You are responsible for handling your own mod's proper lazy-val cleanup
        }
    }

    @SubscribeEvent
    public static void onDeathEvent(LivingDeathEvent event) {
        //Also look into onplayercloned events if you wish to keep cap-data persisted between death or dimension transfers
        if (event.getEntityLiving() instanceof Player) {
            Entity player = event.getEntityLiving();
            //START NOTE
            //This is the proper way to check/modify/edit/etc capability values
            //Verify if it's present then act on via the varible-name we provide, such as "h"
            player.getCapability(CharmAndNailSoulCapability.Provider.PLAYER_SOUL).ifPresent(s -> {
              {
                  //For a future mechanic, capping max soul at half until player finds their shade
                 // s.setMaxSoul(13);
                  s.setSoul(0);
              s.setFocusing(0);
                  s.setFocusingTime(0F);
               }
            });
            //END NOTE
        }
    }



}

