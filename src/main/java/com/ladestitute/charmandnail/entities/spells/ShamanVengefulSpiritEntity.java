package com.ladestitute.charmandnail.entities.spells;

import com.ladestitute.charmandnail.registry.EntityTypeInit;
import com.ladestitute.charmandnail.registry.ItemInit;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.network.protocol.Packet;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.item.Items;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkHooks;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

@SuppressWarnings("EntityConstructor")
public class ShamanVengefulSpiritEntity extends ThrowableItemProjectile {
    private Object Player;

    // Three constructors, also make sure not to miss this line when altering it for copy-pasting
    public ShamanVengefulSpiritEntity(EntityType<ShamanVengefulSpiritEntity> type, Level world) {
        super(type, world);
    }

    public ShamanVengefulSpiritEntity(LivingEntity entity, Level world) {
        super(EntityTypeInit.SHAMAN_VENGEFUL_SPIRIT.get(), entity, world);
    }

    public ShamanVengefulSpiritEntity(double x, double y, double z, Level world) {
        super(EntityTypeInit.SHAMAN_VENGEFUL_SPIRIT.get(), x, y, z, world);
    }

    // Get the item that the projectile is thrown from, blocks require ".asItem()" as well
    @Override
    protected Item getDefaultItem() {
        return ItemInit.VENGEFUL_SPIRIT.get().asItem();
    }

    // Spawns the entity, just as important as the above method
    @Override
    public Packet<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    // A method to do things on entity or block-hit
    @Override
    protected void onHit(HitResult result) {
        //This line is checking the type of RayTraceResult, in this case
        //it will be when it hits and entity
        if (result.getType() == HitResult.Type.ENTITY) {
            //This is a variable that we have set, it gets the entity from the RayTraceResult.
            //We cast it to EntityRayTraceResult, just to ensure that it is infact an entity.
            Entity entity = ((EntityHitResult) result).getEntity();
            //This integer is the damage value that it gives to the entity when it is hit
            //I haven't initialized it here as I will do that below.
            float damage = 13F;

            entity.hurt(DamageSource.thrown(this, this.getOwner()), damage);

            //ItemStack stack1 = new ItemStack(ItemInit.GLASS_SHARD.get());
            // ItemEntity rock = new ItemEntity(this.getCommandSenderWorld(), this.getX(), this.getY() + 1, this.getZ(), stack1);
            //level.addFreshEntity(rock);
            if (!level.isClientSide) {
                this.discard();
            }
        }

        //Just like before this checks the result and if it hits a block this code will run
        if (result.getType() == HitResult.Type.BLOCK) {
            // ItemStack stack1 = new ItemStack(ItemInit.GLASS_SHARD.get());
            // ItemEntity rock = new ItemEntity(this.getCommandSenderWorld(), this.getX(), this.getY() + 1, this.getZ(), stack1);
            // level.addFreshEntity(rock);
            this.discard();
            //Now we get the BlockRayTraceResult from the result
            //Casting it to the BlockRayTraceResult.
            BlockHitResult blockRTR = (BlockHitResult) result;

            //I have checked to see if it hits the top of the block

            //    if (blockRTR.getFace() == Direction.UP) {
            //Then I have added a small check here to only allow something to happen when it
            //Hits a grass block
            //  if (world.getBlockState(blockRTR.getPos()) == Blocks.GRASS_BLOCK.getDefaultState()) {
            //This gets the world, and then sets the blockstate of the position of the entity
            //and the blockstate
            //  world.setBlockState(this.getOnPosition(), Blocks.STONE.getDefaultState());
            //     }

            //And just incase non of these are true, I am removing it from the world.
            if (!level.isClientSide) {
                this.discard();
            }
        }
    }
}

